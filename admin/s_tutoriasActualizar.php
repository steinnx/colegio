<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Colegio Tenze&ntilde;o</title>

        <!-- Bootstrap Core CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <script src="js/buscarAdmin.js"></script>

        <?php
        include_once './sesionStartConexionAdmin.php';
        ?>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"><?php echo $tipo_usuario . ": " . $nombre_usuario; ?></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Opciones<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="perfil.php"><i class="fa fa-fw fa-user"></i> Perfil</a>
                            </li>
                            <li>
                                <a href="../index.php"><i class="fa fa-fw fa-desktop"></i> Pagina</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="../procesos/logout.php"><i class="fa fa-fw fa-power-off"></i> CerrarSesion</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <?php include_once './menuOpciones.php'; ?>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <?php
                $id_getTutoria = $_GET["dato"];
                include_once '../entidades/Dao_Tutorias.php';
                $listar = new DaoTutorias();
                $result = $listar->ListarDetallePorTutoria($id_getTutoria);
                while ($row = mysqli_fetch_array($result)) {
                    $a_fech = $row[0];
                    $a_estado = $row[1];
                    $a_profesor = $row[2];
                    $a_asig = $row[3];
                    $a_alumno = $row[4];
                    $a_valor = $row[5];
                }
                ?>
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <h2>Actualizar Tutoria</h2><br>
                    </div>
                    <form action="procesos/procesoActualizarTutoriasSecretaria.php" method="post" >
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Usuario</label>
                                <input id="txt_usuario" value="<?php echo $a_alumno; ?>"name="txt_usuario"  readonly="readonly"
                                       type="text"  class="form-control input-md">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Profesor</label>
                                <input id="txt_usuario" value="<?php echo $a_profesor; ?>"name="txt_usuario"  readonly="readonly"
                                       type="text"  class="form-control input-md">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Asignatura</label>
                                <input id="txt_usuario" value="<?php echo $a_asig; ?>"name="txt_usuario"  readonly="readonly"
                                       type="text"  class="form-control input-md">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Fecha</label>
                                <input id="txt_usuario" value="<?php echo $a_fech; ?>"name="txt_usuario"  readonly="readonly"
                                       type="text"  class="form-control input-md">
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label" for="name">Valor</label>
                                <input id="txt_usuario" value="<?php echo $a_valor; ?>"name="txt_usuario"  readonly="readonly"
                                       type="text"  class="form-control input-md">
                            </div>
                        </div> 
                        <div class="col-md-1">
                            <div class="form-group">
                                <label class="control-label" for="name">Id</label>
                                <input id="txt_usuario" value="<?php echo $id_getTutoria; ?>"name="txt_id"  readonly="readonly"
                                       type="text"  class="form-control input-md">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Estado</label><br>
                                <select class="form-control" name="estado">
                                    <?php
                                    switch ($a_estado) {
                                        case "Agendada":
                                            echo '<option value="Agendada">Agendada</option>';
                                            echo '<option value="Confirmada">Confirmada</option>';
                                            echo '<option value="Anulada">Anulada</option>';
                                            echo '<option value="Perdida">Perdida</option>';
                                            echo '<option value="Realizada">Realizada</option>';
                                            break;
                                        case "Confirmada":
                                            echo '<option value="Confirmada">Confirmada</option>';
                                            echo '<option value="Agendada">Agendada</option>';
                                            echo '<option value="Anulada">Anulada</option>';
                                            echo '<option value="Perdida">Perdida</option>';
                                            echo '<option value="Realizada">Realizada</option>';
                                            break;
                                        case "Anulada":
                                            echo '<option value="Anulada">Anulada</option>';
                                            echo '<option value="Confirmada">Confirmada</option>';
                                            echo '<option value="Agendada">Agendada</option>';
                                            echo '<option value="Perdida">Perdida</option>';
                                            echo '<option value="Realizada">Realizada</option>';
                                            break;
                                        case "Perdida":
                                            echo '<option value="Perdida">Perdida</option>';
                                            echo '<option value="Confirmada">Confirmada</option>';
                                            echo '<option value="Anulada">Anulada</option>';
                                            echo '<option value="Agendada">Agendada</option>';
                                            echo '<option value="Realizada">Realizada</option>';
                                            break;
                                        case "Realizada":
                                            echo '<option value="Realizada">Realizada</option>';
                                            echo '<option value="Confirmada">Confirmada</option>';
                                            echo '<option value="Anulada">Anulada</option>';
                                            echo '<option value="Perdida">Perdida</option>';
                                            echo '<option value="Agendada">Agendada</option>';
                                            break;

                                        default:
                                            break;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button id="btn_reservar" name="btn_reservar" class="btn btn-default">Actualizar Tutoria</button>
                                <div>
                                    <p class="text-info" id="msgerror"></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>   
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
        <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
        <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>

    </body>

</html>



