<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Colegio Tenze&ntilde;o</title>

        <!-- Bootstrap Core CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <script src="fusioncharts/fusioncharts.js"></script>
        <script src="js/buscarAdmin.js"></script>
        <?php
        include_once './sesionStartConexionAdmin.php';
        ?>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"><?php echo $tipo_usuario . ": " . $nombre_usuario; ?></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Opciones<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="perfil.php"><i class="fa fa-fw fa-user"></i> Perfil</a>
                            </li>
                            <li>
                                <a href="../index.php"><i class="fa fa-fw fa-desktop"></i> Pagina</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="../procesos/logout.php"><i class="fa fa-fw fa-power-off"></i> CerrarSesion</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <?php include_once './menuOpciones.php'; ?>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">
                    <?php
                    include_once './js/fusioncharts.php';
                    ;
                    ?>
                    <div class="col-lg-12">
                        <h2 class="page-header">Estadisticas Tutorias</h2>
                    </div>
                    <div class="col-lg-6">
                        <label>Tutorias por Asignatura</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Asignatura</th>
                                    <th>Numero Tutorias</th>
                                    <th>Porcentaje Tutorias</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT p.asignatura 
                                    FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) 
                                    INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) 
                                    where t.estado='Realizada'");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT p.asignatura,count(t.idTutoria) as tutorias "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) "
                                        . "where t.estado='Realizada' group by p.asignatura DESC");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    echo '<td>' . $aux_profe . '</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <label>Tutorias por Profesor</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Profesor</th>
                                    <th>Numero Tutorias</th>
                                    <th>Porcentaje Tutorias</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT p.nombre_completo 
                                    FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) 
                                    INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) 
                                    where t.estado='Confirmada'");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT p.nombre_completo,count(t.idTutoria) as tutorias "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) "
                                        . "where t.estado='Confirmada' group by p.idProfesor DESC");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    echo '<td>' . $aux_profe . '</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <label>Tutorias por Estado</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Numero Tutorias</th>
                                    <th>Porcentaje Tutorias</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT t.estado FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) ");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT t.estado,count(t.idTutoria) as tutorias "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) "
                                        . "group by t.estado DESC");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    echo '<td>' . $aux_profe . '</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <label>Tutorias por Mes</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Mes</th>
                                    <th>Numero Tutorias</th>
                                    <th>Porcentaje Tutorias</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT t.idTutoria "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno)"
                                        . "where t.estado='Realizada' ");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT MONTH(t.fecha_tutoria) as mes,count(t.idTutoria) "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno)"
                                        . "where t.estado='Realizada' "
                                        . "group by MONTH(t.fecha_tutoria) ");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    switch ($aux_profe) {
                                        case 1:
                                            $aux_profe = 'Enero';
                                            break;
                                        case 2:
                                            $aux_profe = 'Febrero';
                                            break;
                                        case 3:
                                            $aux_profe = 'Marzo';
                                            break;
                                        case 4:
                                            $aux_profe = 'Abril';
                                            break;
                                        case 5:
                                            $aux_profe = 'Mayo';
                                            break;
                                        case 6:
                                            $aux_profe = 'Junio';
                                            break;
                                        case 7:
                                            $aux_profe = 'Julio';
                                            break;
                                        case 8:
                                            $aux_profe = 'Agosto';
                                            break;
                                        case 9:
                                            $aux_profe = 'Septiembre';
                                            break;
                                        case 10:
                                            $aux_profe = 'Octubre';
                                            break;
                                        case 11:
                                            $aux_profe = 'Noviembre';
                                            break;
                                        case 12:
                                            $aux_profe = 'Diciembre';
                                            break;
                                        default:
                                            break;
                                    }
                                    echo '<td>' . $aux_profe . '</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <label>Tutorias por Fechas</label>
                        <form action="d_estadisticasRangoFecha.php" method="post">
                            <div class="form-group">
                                <label class="control-label" for="name">Fecha Comienzo</label>
                                <input id="txt_fecha_nacimiento" required=""
                                       name="txt_fecha_comienzo"  type="date" class="form-control input-md">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="name">Fecha Final</label>
                                <input id="txt_fecha_nacimiento" required=""
                                       name="txt_fecha_final"  type="date" class="form-control input-md">
                                <br>
                                <div class="form-group">
                                    <button id="btn_reservar" name="btn_reservar" class="btn btn-default">Filtrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12">
                        <h2 class="page-header">Estadisticas Alumnos</h2>
                    </div>
                    <div class="col-lg-3">
                        <label>Alumnos por Curso</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Curso</th>
                                    <th>Numero Alumnos</th>
                                    <th>Porcentaje Alumnos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT * FROM alumno ");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT c.descripcion,count(c.idCurso) FROM alumno a inner join curso c on(a.idCurso=c.idCurso) GROUP BY a.idCurso");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    echo '<td>' . $aux_profe . '</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-3">
                        <label>Tutorias Realizadas</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Alumno</th>
                                    <th>Cantidad Tutorias</th>
                                    <th>Porcentaje Tutorias</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT t.idTutoria "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno)"
                                        . "where t.estado='Realizada' ");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT a.nombre_completo,count(t.idTutoria) "
                                        . "FROM tutoria t inner join profesor p on (t.idProfesorTutoria=p.idProfesor) "
                                        . "INNER join alumno a on (t.idAlumnoTutoria=a.idAlumno) "
                                        . "where t.estado='Realizada' group by a.nombre_completo");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    echo '<td>' . $aux_profe . '</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-5">
                        <label>Rango Etario</label>
                        <table  id="regTable" class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th>Edad</th>
                                    <th>Cantidad Alumnos</th>
                                    <th>Porcentaje Alumnos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($link, "SELECT * FROM alumno ");
                                $contador = mysqli_num_rows($query);
                                $result = mysqli_query($link, "SELECT a.edad,count(a.idAlumno) 
                                    FROM alumno a GROUP BY a.edad");
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    $aux_profe = $row[0];
                                    $aux_turo = $row[1];
                                    echo '<td>' . $aux_profe . ' Años</td>';
                                    echo '<td>' . $aux_turo . '</td>';
                                    $num = $aux_turo / $contador;
                                    $num = number_format($num * 100, 2, ",", ".") . " %";
                                    echo '<td>' . $num . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
        <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
        <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>

    </body>

</html>

