<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Colegio Tenze&ntilde;o</title>

        <!-- Bootstrap Core CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="startbootstrap-sb-admin-gh-pages/css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="startbootstrap-sb-admin-gh-pages/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <script src="js/buscarAdmin.js"></script>
        <?php
        include_once './sesionStartConexionAdmin.php';
        ?>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"><?php echo $tipo_usuario . ": " . $nombre_usuario; ?></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Opciones<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="perfil.php"><i class="fa fa-fw fa-user"></i> Perfil</a>
                            </li>
                            <li>
                                <a href="../index.php"><i class="fa fa-fw fa-desktop"></i> Pagina</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="../procesos/logout.php"><i class="fa fa-fw fa-power-off"></i> CerrarSesion</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <?php include_once './menuOpciones.php'; ?>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <h2>Nuevo Profesor</h2><br>
                    </div>
                    <form action="procesos/procesoAgregarProfesorInspector.php" method="post" >
                        <!-- Form start -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="name">Usuario</label>
                                <input id="txt_usuario" name="txt_usuario"  required=""
                                       type="text"  class="form-control input-md">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Rut</label>
                                <input id="txt_rut" name="txt_rut"  placeholder="Ingrese Rut Ej. 11.111.111-1"
                                       required="" pattern="[0-9]{2}.[0-9]{3}.[0-9]{3}-[0-9kK]"
                                       type="text" class="form-control input-md">
                            </div>
                        </div>
                        <!--<div class="col-lg-2">
                            <div class="form-group">
                                <label class="control-label" for="name">Digito Verificador</label>
                                <input id="txt_rut" name="txt_verificador"  readonly="redonly"type="number" class="form-control input-md">
                            </div>
                        </div>-->
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label class="control-label" for="name">Nombre Completo</label>
                                <input id="txt_nombre_completo" required=""
                                       name="txt_nombre_completo"  type="text" class="form-control input-md">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Fecha Contratacion</label>
                                <input id="txt_fecha_contratacion" required=""
                                       name="txt_fecha_contratacion"  type="date" class="form-control input-md">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Asignatura</label><br>
                                <select class="form-control" name="asignatura">
                                    <option value="Matematicas">Matematicas</option>
                                    <option value="Lenguaje">Lenguaje</option>
                                    <option value="Ingles">Ingles</option>
                                    <option value="Historia">Historia</option>
                                    <option value="Tecnologia">Tecnologia</option>
                                    <option value="Artes Visuales">Artes Visuales</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label" for="name">Valor Tutoria</label>
                                <input id="txt_valor_tutoria" required=""
                                       name="txt_valor_tutoria"  type="number" class="form-control input-md">
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <button id="btn_reservar" name="btn_reservar" class="btn btn-default">Nuevo Profesor</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="startbootstrap-sb-admin-gh-pages/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="startbootstrap-sb-admin-gh-pages/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/raphael.min.js"></script>
    <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris.min.js"></script>
    <script src="startbootstrap-sb-admin-gh-pages/js/plugins/morris/morris-data.js"></script>

</body>

</html>