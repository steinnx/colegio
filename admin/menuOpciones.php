<?php

echo '    <li>
                 <a href="index.php"><i class="fa fa-fw fa-desktop"></i> Inicio</a>
              </li>';
if (isset($_SESSION['tipoUsuario']) && $_SESSION['tipoUsuario'] == 'Director') {
    echo '    <li>
                 <a href="d_alumnos.php"><i class="fa fa-fw fa-users"></i> Alumnos</a>
              </li>
              <li>
                 <a href="d_profesores.php"><i class="fa fa-fw fa-users"></i> Profesores</a>
              </li>
              <li>
                 <a href="d_tutorias.php"><i class="fa fa-fw fa-book"></i> Tutorias</a>
              </li>
      <li>
      <a href="d_estadisticas.php"><i class="fa fa-fw fa-bar-chart-o"></i> Estadisticas</a>
      </li>';
}
if (isset($_SESSION['tipoUsuario']) && $_SESSION['tipoUsuario'] == 'Inspector') {
    echo '    <li>
                 <a href="i_alumnos.php"><i class="fa fa-fw fa-users"></i> Alumnos</a>
              </li>
              <li>
                 <a href="i_profesores.php"><i class="fa fa-fw fa-users"></i> Profesores</a>
              </li>
              <li>
                 <a href="i_usuario.php"><i class="fa fa-fw fa-users"></i> Usuarios</a>
              </li>
              ';
}
if (isset($_SESSION['tipoUsuario']) && $_SESSION['tipoUsuario'] == 'Secretaria') {
    echo '    <li>
                 <a href="s_alumnos.php"><i class="fa fa-fw fa-users"></i> Alumnos</a>
              </li>
              <li>
                 <a href="s_profesores.php"><i class="fa fa-fw fa-users"></i> Profesores</a>
              </li>
              <li>
                 <a href="s_tutorias.php"><i class="fa fa-fw fa-book"></i> Tutorias</a>
              </li>
              <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-book"></i> Tutorias Evaluar <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="s_agendarTutorias.php">Agendar</a>
                            </li>
                            <li>
                                <a href="s_tutoriasAdministrar.php">Administrar</a>
                            </li>
                        </ul>
                    </li>              

';
}
if (isset($_SESSION['tipoUsuario']) && $_SESSION['tipoUsuario'] == 'Alumno') {
    echo '    
              <li>
                 <a href="a_tutorias.php"><i class="fa fa-fw fa-book"></i> Tutorias</a>
              </li>';
}

if (isset($_SESSION['tipoUsuario']) && $_SESSION['tipoUsuario'] == 'Profesor') {
    echo '    
              <li>
                 <a href="p_tutorias.php"><i class="fa fa-fw fa-book"></i> Tutorias</a>
              </li>';
}