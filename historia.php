<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home Colegio Tenzeño</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/startbootstrap-clean-blog-gh-pages/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/startbootstrap-clean-blog-gh-pages/css/clean-blog.min.css" rel="stylesheet">


        <link href="css/startbootstrap-clean-blog-gh-pages/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <?php
        include_once './sesionStartConexion.php';
        ?>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        Menu <i class="fa fa-bars"></i>
                    </button>
                    <a href="index.php"><img src="img/Logo.png" /></a>
                    <!--<a class="navbar-brand" href="index.php">Inicio</a>-->
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="historia.php">Historia</a>
                        </li>
                        <li>
                            <a href="ubicacion.php">Ubicacion</a>
                        </li>
                        <?php include_once './menuLogin.php';?>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Header -->
        <!-- Set your background image for this header on the line below. -->
        <header class="intro-header" style="background-image: url('img/fondo2.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <div class="site-heading">
                            <h1>Historia del Colegio</h1>
                            <hr class="small">
                            <span class="subheading">Colegio Tenze&ntilde;o </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container text-center">
            <div class="row">

                <h2>Colegio Tenze&ntilde;o</h2>
                <p>
                    El Colegio Tenze&ntildeo, es una institución educativa dependiente de la Sociedad Educacional San Nicolás, fundada el 6 de julio de 1976 por la educadora Sra. Elena Montino Johnson.                    </p>
                <p>
                    Nuestro Colegio busca el realizar un aporte sustantivo a la educación de los niños y jovenes chilenos, ofreciéndoles una educación de calidad, fundada en los requerimientos de la vida contemporánea. Por lo anterior, cuenta con un afiatado cuerpo docente con los adecuados niveles de especialización en la tarea que desarrollan                    </p>
                <p>
                    En apoyo de la labor docente el Colegio cuenta con una óptima infraestructura, con amplios espacios para la práctica deportiva y recreativa, siendo uno de sus pilares distintivos, la utilización de tecnología de punta en informática, contando con planes y programas propios en computación, asignatura obligatoria a contar de 5º básico. Además, cada una de las asignaturas cuenta con un respaldo en software y material didáctico de primer nivel, lo que sin duda debe conducir al colegio a alcanzar los más altos estándares de aprendizaje en sus alumnos.
                </p>
            </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

                        <p class="copyright text-muted">Copyright &copy; Colegio Tenze&ntilde;o.</p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- jQuery -->
        <script src="css/startbootstrap-clean-blog-gh-pages/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="css/startbootstrap-clean-blog-gh-pages/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="css/startbootstrap-clean-blog-gh-pages/js/jqBootstrapValidation.js"></script>
        <script src="css/startbootstrap-clean-blog-gh-pages/js/contact_me.js"></script>

        <!-- Theme JavaScript -->
        <script src="css/startbootstrap-clean-blog-gh-pages/js/clean-blog.min.js"></script>

    </body>

</html>

