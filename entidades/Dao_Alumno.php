<?php

include_once 'CL_Conexion.php';

class DaoAlumno{
    private $cone;
    
    public function DaoAlumno() {
        try {
            $this->cone = new Cl_Conexion();
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }
    public function EliminarPorIdAlumno($idAlumno) {
        try {
            $sql = "delete from alumno where idAlumno=$idAlumno";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ingresarAlumno($nom,$fech,$rut,$tel,$dir,$idcur,$iduse) {
        try {
            $sql = "insert into alumno(nombre_completo,fecha_nacimiento,rut,telefono,direccion,idCurso,idUsuario) "
                    . "values('$nom','$fech','$rut','$tel','$dir','$idcur','$iduse')";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function buscarAlumno($idAlumno) {
        try {
            $sql = "SELECT * FROM alumno where idAlumno=$idAlumno;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function Listar() {
        try {
            $sql = "SELECT * FROM alumno a inner join curso c on (a.idCurso=c.idCurso) ORDER by c.descripcion desc;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}

