<?php

class CL_Alumno {

    private $idAlumno;
    private $nombre_completo;
    private $rut;
    private $fecha_nacimiento;
    private $telefono;
    private $direccion;
    private $idUsuario;

    function __construct() {
        
    }

    function getIdAlumno() {
        return $this->idAlumno;
    }

    function getNombre_completo() {
        return $this->nombre_completo;
    }

    function getRut() {
        return $this->rut;
    }

    function getFecha_nacimiento() {
        return $this->fecha_nacimiento;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function setIdAlumno($idAlumno) {
        $this->idAlumno = $idAlumno;
    }

    function setNombre_completo($nombre_completo) {
        $this->nombre_completo = $nombre_completo;
    }

    function setRut($rut) {
        $this->rut = $rut;
    }

    function setFecha_nacimiento($fecha_nacimiento) {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

}
