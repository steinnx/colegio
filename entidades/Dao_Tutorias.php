<?php

include_once 'CL_Conexion.php';

class DaoTutorias{
    private $cone;
    
    public function DaoTutorias() {
        try {
            $this->cone = new Cl_Conexion();
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }
    public function EliminarPorIdTutorias($idTutorias) {
        try {
            $sql = "delete from tutoria where idTutoria=$idTutorias";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ActualizarPorIdTutorias($idTutorias,$estado) {
        try {
            $sql = "update tutoria set estado='$estado' where idTutoria=$idTutorias";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function Listar() {
        try {
            $sql = "select * from tutoria";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ListarDetalle() {
        try {
            $sql = "SELECT t.fecha_tutoria,t.estado
                ,p.nombre_completo,p.asignatura,a.nombre_completo
                ,p.valortutoria,t.idTutoria
                FROM tutoria t 
                inner join profesor p 
                on (t.idProfesorTutoria=p.idProfesor) 
                INNER join alumno a on 
                (t.idAlumnoTutoria=a.idAlumno) order by t.fecha_tutoria DESC;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ListarDetallePorAlumno($idAlumno) {
        try {
            $sql = "SELECT t.fecha_tutoria,t.estado
                ,p.nombre_completo,p.asignatura,a.nombre_completo
                ,p.valortutoria 
                FROM tutoria t 
                inner join profesor p 
                on (t.idProfesorTutoria=p.idProfesor) 
                INNER join alumno a on 
                (t.idAlumnoTutoria=a.idAlumno) where a.idAlumno=$idAlumno;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ListarDetallePorTutoria($idAlumno) {
        try {
            $sql = "SELECT t.fecha_tutoria,t.estado
                ,p.nombre_completo,p.asignatura,a.nombre_completo
                ,p.valortutoria 
                FROM tutoria t 
                inner join profesor p 
                on (t.idProfesorTutoria=p.idProfesor) 
                INNER join alumno a on 
                (t.idAlumnoTutoria=a.idAlumno) where t.idTutoria=$idAlumno;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ListarDetallePorProfesor($idProfesor) {
        try {
            $sql = "SELECT t.fecha_tutoria,t.estado,p.nombre_completo,p.asignatura,a.nombre_completo,p.valortutoria FROM tutoria t 
                inner join profesor p on (t.idProfesorTutoria=p.idProfesor) 
                inner join alumno a on(t.idAlumnoTutoria=t.idAlumnoTutoria) where p.idProfesor=$idProfesor order by t.fecha_tutoria desc;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}

