<?php
class CL_Director {

    private $idDirector;
    private $nombre_completo;
    private $rut;
    private $fecha_contratacion;
    private $correo;
    private $idUsuario;

    function __construct() {
        
    }

    function getIdDirector() {
        return $this->idDirector;
    }

    function getNombre_completo() {
        return $this->nombre_completo;
    }

    function getRut() {
        return $this->rut;
    }

    function getFecha_contratacion() {
        return $this->fecha_contratacion;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function setIdDirector($idDirector) {
        $this->idDirector = $idDirector;
    }

    function setNombre_completo($nombre_completo) {
        $this->nombre_completo = $nombre_completo;
    }

    function setRut($rut) {
        $this->rut = $rut;
    }

    function setFecha_contratacion($fecha_contratacion) {
        $this->fecha_contratacion = $fecha_contratacion;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }


}

