<?php
class Cl_Tutoria{
    private $idTutoria;
    private $fecha_tutoria;
    private $estado;
    private $idAlumnoTutoria;
    private $idProfesorTutoria;
    
    function __construct() {
        
    }

    function getIdTutoria() {
        return $this->idTutoria;
    }

    function getFecha_tutoria() {
        return $this->fecha_tutoria;
    }

    function getEstado() {
        return $this->estado;
    }

    function getIdAlumnoTutoria() {
        return $this->idAlumnoTutoria;
    }

    function getIdProfesorTutoria() {
        return $this->idProfesorTutoria;
    }

    function setIdTutoria($idTutoria) {
        $this->idTutoria = $idTutoria;
    }

    function setFecha_tutoria($fecha_tutoria) {
        $this->fecha_tutoria = $fecha_tutoria;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setIdAlumnoTutoria($idAlumnoTutoria) {
        $this->idAlumnoTutoria = $idAlumnoTutoria;
    }

    function setIdProfesorTutoria($idProfesorTutoria) {
        $this->idProfesorTutoria = $idProfesorTutoria;
    }


}

