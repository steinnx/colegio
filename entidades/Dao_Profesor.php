<?php

include_once 'CL_Conexion.php';

class DaoProfesor{
    private $cone;
    
    public function DaoProfesor() {
        try {
            $this->cone = new Cl_Conexion();
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }
    public function EliminarPorIdProfesor($idProfesor) {
        try {
            $sql = "delete from profesor where idProfesor=$idProfesor";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function buscarProfesor($idProfesor) {
        try {
            $sql = "select * from profesor where idProfesor=$idProfesor";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function Listar() {
        try {
            $sql = "select * from profesor";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}

