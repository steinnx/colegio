<?php

class Cl_Conexion {

    private $host = "localhost";
    private $usuario = "root";
    private $password = "";
    private $base = "colegio";
    private $cone; //objeto de tipo conexion
    /*
    private $host = "mysql.hostinger.es";
    private $usuario = "u536046640_cent";
    private $password = "centrodeeventos";
    private $base = "u536046640_cent";
    private $cone; //objeto de tipo conexion*/
    public function Cl_Conexion() {
        try {
            $this->cone = mysqli_connect($this->host, $this->usuario, $this->password, $this->base);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    // metodos para insertar-eliminar-modificar
    public function sqlOperaciones($sql) {
        try {
            $resp = mysqli_query($this->cone, $sql);
            return mysqli_affected_rows($this->cone);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function sqlSeleccion($sql) {
        try {
            $resp = mysqli_query($this->cone, $sql);
            return $resp;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}

