<?php

include_once '../entidades/CL_Conexion.php';

class DaoCurso{
    private $cone;
    
    public function DaoCurso() {
        try {
            $this->cone = new Cl_Conexion();
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }
    public function EliminarPorIdCurso($idCurso) {
        try {
            $sql = "delete from curso where idCurso=$idCurso;";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function Listar() {
        try {
            $sql = "SELECT * FROM curso;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function ListarSigla() {
        try {
            $sql = "SELECT sigla FROM curso;";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}

