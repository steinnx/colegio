<?php

include_once 'CL_Conexion.php';

class DaoUsuario {

    private $cone;

    public function DaoUsuario() {
        try {
            $this->cone = new Cl_Conexion();
        } catch (Exception $ex) {
            echo $ex->getTraceAsString();
        }
    }

    public function ingresarUsuarioTipo($nombre, $tipo) {
        try {
            switch ($tipo) {
                case "Alumno":
                    $sql = "insert into usuario(usuario,pass,tipo_usuario) "
                            . "values('$nombre','c6865cf98b133f1f3de596a4a2894630','$tipo')";
                    break;
                case "Director":
                    $sql = "insert into usuario(usuario,pass,tipo_usuario) "
                            . "values('$nombre','3d4e992d8d8a7d848724aa26ed7f4176','$tipo')";
                    break;
                case "Secretaria":
                    $sql = "insert into usuario(usuario,pass,tipo_usuario) "
                            . "values('$nombre','fd09accffacf03d7393c2a23a9601b43','$tipo')";
                    break;
                case "Inspector":
                    $sql = "insert into usuario(usuario,pass,tipo_usuario) "
                            . "values('$nombre','8e96c1fb87ac069c2a39f1ed61b10428','$tipo')";
                    break;
                case "Profesor":
                    $sql = "insert into usuario(usuario,pass,tipo_usuario) "
                            . "values('$nombre','793741d54b00253006453742ad4ed534','$tipo')";
                    break;
                default:
                    break;
            }
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    public function selectMaxId(){
        try {
            $sql = "select MAX(id)as maximo from Usuario";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function EliminarPorIdUsuario($idUsuario) {
        try {
            $sql = "delete from Usuario where idUsuario=$idUsuario";
            return $this->cone->sqlOperaciones($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function Listar() {
        try {
            $sql = "select * from usuario";
            return $this->cone->sqlSeleccion($sql);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
