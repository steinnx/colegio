-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-07-2017 a las 07:25:18
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colegio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `idAlumno` int(255) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `rut` int(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `idCurso` int(255) NOT NULL,
  `idUsuario` int(255) NOT NULL,
  `edad` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`idAlumno`, `nombre_completo`, `fecha_nacimiento`, `rut`, `telefono`, `direccion`, `idCurso`, `idUsuario`, `edad`) VALUES
(4, 'Jorge Reyes', '1993-07-03', 18366563, '+56912312312', '63HCZeNPfVmJoZchJ5Ws+oUUHlAY/4ja+l4CtvyDCsg=', 2, 9, 12),
(5, 'Alexis Sanchez', '2006-01-28', 15345365, '+56912312312', 'JRAAYT27P3dRWRKwipaouzMIv9gh6OKVYwvLJUHTwdk=', 1, 10, 11),
(6, 'Daniel Herrera', '2004-07-04', 13123423, '+56912312312', 'Y8t5Teq3IKP9U1sTOt92KkOxAhXJ2OZFt+CbYcCnJ1M=', 3, 11, 13),
(7, 'Pedro Hernandez', '2005-06-14', 12365564, '+56912312312', 'JRAAYT27P3dRWRKwipaouzMIv9gh6OKVYwvLJUHTwdk=', 2, 14, 12),
(8, 'Axel Duran', '2004-06-08', 17326563, '+56912343232', 'lqXePMLQV0U5npmZY7CnVfAjnccNGdXDgiNB1YOjA+0=', 3, 15, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `idCurso` int(255) NOT NULL,
  `sigla` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`idCurso`, `sigla`, `descripcion`) VALUES
(1, '5°Basico', 'Quinto Basico'),
(2, '6°Basico', 'Sexto Basico'),
(3, '7°Basico', 'Septimo Basico'),
(4, '8°Basico', 'Octavo Basico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `director`
--

CREATE TABLE `director` (
  `idDirector` int(255) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `rut` int(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `idUsuario` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `director`
--

INSERT INTO `director` (`idDirector`, `nombre_completo`, `fecha_contratacion`, `rut`, `correo`, `idUsuario`) VALUES
(1, 'Jorge Orellana', '2012-06-13', 10975914, 'director@tenzeno.cl', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inspector`
--

CREATE TABLE `inspector` (
  `idInspector` int(255) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `rut` int(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `idUsuario` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inspector`
--

INSERT INTO `inspector` (`idInspector`, `nombre_completo`, `fecha_contratacion`, `rut`, `correo`, `idUsuario`) VALUES
(1, 'Francisco Sepulveda', '2011-06-04', 9397198, 'inspector@tenzeno.cl', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idProfesor` int(255) NOT NULL,
  `rut` int(255) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `asignatura` varchar(255) NOT NULL,
  `valortutoria` int(255) NOT NULL,
  `idUsuario` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`idProfesor`, `rut`, `nombre_completo`, `fecha_contratacion`, `asignatura`, `valortutoria`, `idUsuario`) VALUES
(1, 19428250, 'Alexis Inzulza', '2011-06-13', 'Matematicas', 20000, 6),
(2, 12811358, 'Juan Rodriguez', '2012-06-12', 'Lenguaje', 20000, 7),
(3, 18366563, 'Israel AvendaÃ±o', '2017-07-09', 'Matematicas', 3000, 12),
(4, 11235563, 'Michelle Salgado', '2017-07-09', 'Artes Visuales', 2300, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secretaria`
--

CREATE TABLE `secretaria` (
  `idSecretaria` int(255) NOT NULL,
  `nombre_completo` varchar(255) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `rut` int(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `idUsuario` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `secretaria`
--

INSERT INTO `secretaria` (`idSecretaria`, `nombre_completo`, `fecha_contratacion`, `rut`, `correo`, `idUsuario`) VALUES
(1, 'Alejandra Rebolledo', '2012-06-13', 22539808, 'secretaria@tenzeno.cl', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutoria`
--

CREATE TABLE `tutoria` (
  `idTutoria` int(255) NOT NULL,
  `fecha_tutoria` date NOT NULL,
  `estado` varchar(255) NOT NULL,
  `idProfesorTutoria` int(255) NOT NULL,
  `idAlumnoTutoria` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tutoria`
--

INSERT INTO `tutoria` (`idTutoria`, `fecha_tutoria`, `estado`, `idProfesorTutoria`, `idAlumnoTutoria`) VALUES
(5, '2017-06-01', 'Anulada', 1, 4),
(6, '2017-07-05', 'Realizada', 1, 6),
(7, '2017-07-06', 'Realizada', 1, 5),
(8, '2017-06-22', 'Realizada', 2, 4),
(9, '2017-07-19', 'Confirmada', 2, 6),
(10, '2017-06-15', 'Realizada', 2, 5),
(11, '2017-06-15', 'Realizada', 2, 5),
(12, '2017-07-26', 'Confirmada', 2, 5),
(13, '2017-07-29', 'Confirmada', 2, 6),
(14, '2017-07-30', 'Confirmada', 2, 6),
(15, '2017-07-29', 'Confirmada', 2, 5),
(16, '2017-07-21', 'Confirmada', 1, 5),
(17, '2017-07-11', 'Realizada', 3, 6),
(18, '2017-07-20', 'Confirmada', 4, 6),
(19, '2017-07-29', 'Confirmada', 3, 5),
(20, '2017-07-13', 'Confirmada', 4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `tipo_usuario` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `usuario`, `pass`, `tipo_usuario`) VALUES
(1, 'director', '3d4e992d8d8a7d848724aa26ed7f4176', 'Director'),
(2, 'secretaria', 'fd09accffacf03d7393c2a23a9601b43', 'Secretaria'),
(3, 'inspector', '8e96c1fb87ac069c2a39f1ed61b10428', 'Inspector'),
(6, 'profesor_1', '793741d54b00253006453742ad4ed534', 'Profesor'),
(7, 'profesor_2', '793741d54b00253006453742ad4ed534', 'Profesor'),
(9, 'alumno_1', 'c6865cf98b133f1f3de596a4a2894630', 'Alumno'),
(10, 'alumno_2', 'c6865cf98b133f1f3de596a4a2894630', 'Alumno'),
(11, 'alumno_3', 'c6865cf98b133f1f3de596a4a2894630', 'Alumno'),
(12, 'profesor_3', '793741d54b00253006453742ad4ed534', 'Profesor'),
(13, 'profesor_4', '793741d54b00253006453742ad4ed534', 'Profesor'),
(14, 'alumno_4', 'c6865cf98b133f1f3de596a4a2894630', 'Alumno'),
(15, 'alumno_5', 'c6865cf98b133f1f3de596a4a2894630', 'Alumno');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`idAlumno`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idCurso` (`idCurso`),
  ADD KEY `idCurso_2` (`idCurso`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indices de la tabla `director`
--
ALTER TABLE `director`
  ADD PRIMARY KEY (`idDirector`),
  ADD KEY `tipo_usuario` (`idUsuario`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `inspector`
--
ALTER TABLE `inspector`
  ADD PRIMARY KEY (`idInspector`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idProfesor`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `secretaria`
--
ALTER TABLE `secretaria`
  ADD PRIMARY KEY (`idSecretaria`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `tutoria`
--
ALTER TABLE `tutoria`
  ADD PRIMARY KEY (`idTutoria`),
  ADD KEY `idProfesorTutoria` (`idProfesorTutoria`),
  ADD KEY `idAlumnoTutoria` (`idAlumnoTutoria`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumno`
--
ALTER TABLE `alumno`
  MODIFY `idAlumno` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `idCurso` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `director`
--
ALTER TABLE `director`
  MODIFY `idDirector` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `inspector`
--
ALTER TABLE `inspector`
  MODIFY `idInspector` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idProfesor` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `secretaria`
--
ALTER TABLE `secretaria`
  MODIFY `idSecretaria` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tutoria`
--
ALTER TABLE `tutoria`
  MODIFY `idTutoria` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `alumno_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alumno_ibfk_2` FOREIGN KEY (`idCurso`) REFERENCES `curso` (`idCurso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `director`
--
ALTER TABLE `director`
  ADD CONSTRAINT `director_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inspector`
--
ALTER TABLE `inspector`
  ADD CONSTRAINT `inspector_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `profesor_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `secretaria`
--
ALTER TABLE `secretaria`
  ADD CONSTRAINT `secretaria_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tutoria`
--
ALTER TABLE `tutoria`
  ADD CONSTRAINT `tutoria_ibfk_1` FOREIGN KEY (`idProfesorTutoria`) REFERENCES `profesor` (`idProfesor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tutoria_ibfk_2` FOREIGN KEY (`idAlumnoTutoria`) REFERENCES `alumno` (`idAlumno`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
